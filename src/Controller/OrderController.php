<?php

namespace App\Controller;

use App\UseCase\Country\GetAllCountriesUseCase;
use App\UseCase\Order\ChangeOrderStatus\ChangeOrderStatusRequest;
use App\UseCase\Order\ChangeOrderStatus\ChangeOrderStatusResponse;
use App\UseCase\Order\ChangeOrderStatus\ChangeOrderStatusUseCase;
use App\UseCase\Order\GetAllOrders\GetAllOrdersUseCase;
use App\UseCase\OrderState\GetAllOrderStatesUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/order/{showAll}", name="app_order_all")
     */
    public function show(
        GetAllOrdersUseCase      $getAllOrdersUseCase,
        GetAllOrderStatesUseCase $getAllOrderStatesUseCase,
        GetAllCountriesUseCase   $getAllCountriesUseCase,
                                 $showAll = '')
    {
        $orders = $getAllOrdersUseCase->execute((bool)$showAll);
        $status = $getAllOrderStatesUseCase->execute();
        $countries = $getAllCountriesUseCase->execute();

        return $this->render('order/show.html.twig', ['orders' => $orders, 'statusList' => $status, 'countryList' => $countries]);
    }

    /**
     * @Route("/changeStatus", name="app_order_change_status")
     */
    public function changeStatus(Request $request, ChangeOrderStatusUseCase $changeOrderStatusUseCase)
    {
        $orderId = $request->get('orderId');
        $statusId = $request->get('status');

        if (!$orderId || !$statusId) {
            return $this->json(['status' => Response::HTTP_BAD_REQUEST]);
        }

        $changeOrderStatusRequest = new ChangeOrderStatusRequest($orderId, $statusId);
        $response = $changeOrderStatusUseCase->execute($changeOrderStatusRequest);

        if ($response->getStatus() == ChangeOrderStatusResponse::STATUS_KO) {
            return $this->json(['status' => Response::HTTP_BAD_REQUEST]);
        }

        return $this->json(['status' => Response::HTTP_OK]);
    }
}