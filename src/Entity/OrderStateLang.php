<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderStateLang
 *
 * @ORM\Table(name="order_state_lang")
 * @ORM\Entity(repositoryClass="App\Repository\OrderStateLangRepository")
 */
class OrderStateLang
{
    public const PENDING_PAYMENT_STATUS = 1;
    public const ACCEPTED_PAYMENT_STATUS = 2;
    public const PENDING_STATUS = 3;
    public const SENT_STATUS = 4;
    public const DELIVERED_STATUS = 5;
    public const CANCELLED_STATUS = 6;
    public const REFUND_STATUS = 7;
    public const PAYMENT_ERROR_STATUS = 8;

    /**
     * @var int
     *
     * @ORM\Column(name="id_order_state", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderState;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    public function getIdOrderState(): ?int
    {
        return $this->idOrderState;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


}
