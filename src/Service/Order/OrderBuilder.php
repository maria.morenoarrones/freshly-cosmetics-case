<?php

namespace App\Service\Order;

use App\Repository\AddressRepository;
use App\Repository\CountryLangRepository;
use App\Repository\CustomerRepository;
use App\Repository\OrderDetailRepository;
use App\Repository\OrdersRepository;
use App\Repository\OrderStateLangRepository;

class OrderBuilder
{
    private $ordersRepository;
    private $orderDetailRepository;
    private $addressRepository;
    private $countryLangRepository;
    private $customerRepository;
    private $orderStateLangRepository;

    public function __construct(
        OrdersRepository         $ordersRepository,
        OrderDetailRepository    $orderDetailRepository,
        AddressRepository        $addressRepository,
        CountryLangRepository    $countryLangRepository,
        CustomerRepository       $customerRepository,
        OrderStateLangRepository $orderStateLangRepository
    )
    {
        $this->ordersRepository = $ordersRepository;
        $this->orderDetailRepository = $orderDetailRepository;
        $this->addressRepository = $addressRepository;
        $this->countryLangRepository = $countryLangRepository;
        $this->customerRepository = $customerRepository;
        $this->orderStateLangRepository = $orderStateLangRepository;
    }

    public function build(array $orders): array
    {
        $ordersData = [];

        foreach ($orders as $order) {
            $orderData = [];

            $orderAddress = $this->addressRepository->find($order->getIdAddressDelivery());
            $orderCountry = $this->countryLangRepository->find($orderAddress->getIdCountry());
            $orderCustomer = $this->customerRepository->find($order->getIdCustomer());
            $orderProducts = $this->orderDetailRepository->findBy(['idOrder' => $order->getIdOrder()]);
            $orderStatus = $this->orderStateLangRepository->find($order->getCurrentState());

            $orderData['id'] = $order->getIdOrder();
            $orderData['date'] = $order->getDateAdd()->format('Y-m-d H:i:s');
            $orderData['customerName'] = $orderCustomer->getFirstname() . ' ' . $orderCustomer->getLastname();
            $orderData['address'] = $orderAddress->getAddress1() . ', ' . $orderAddress->getAddress2() . ' (' . $orderAddress->getCity() . ')';
            $orderData['country'] = ['id' => $orderCountry->getIdCountry(), 'name' => $orderCountry->getName()];

            $orderData['products'] = [];
            foreach ($orderProducts as $orderProduct) {
                $orderData['products'][$orderProduct->getIdOrderDetail()] = [
                    'quantity' => $orderProduct->getProductQuantity(),
                    'name' => $orderProduct->getProductName()
                ];
            }

            $orderData['status'] = ['id' => $orderStatus->getIdOrderState(), 'name' => $orderStatus->getName()];

            $ordersData[] = $orderData;
        }

        return $ordersData;
    }
}