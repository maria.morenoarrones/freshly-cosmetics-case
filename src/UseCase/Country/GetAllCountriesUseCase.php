<?php

namespace App\UseCase\Country;

use App\Repository\CountryLangRepository;

class GetAllCountriesUseCase
{
    private $countryLangRepository;

    public function __construct(CountryLangRepository $countryLangRepository)
    {
        $this->countryLangRepository = $countryLangRepository;
    }

    public function execute()
    {
        return $this->countryLangRepository->findAll();
    }
}