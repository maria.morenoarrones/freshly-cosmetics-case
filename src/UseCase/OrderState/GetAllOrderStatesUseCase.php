<?php

namespace App\UseCase\OrderState;

use App\Repository\OrderStateLangRepository;

class GetAllOrderStatesUseCase
{
    private $orderStateLangRepository;

    public function __construct(OrderStateLangRepository $orderStateLangRepository)
    {
        $this->orderStateLangRepository = $orderStateLangRepository;
    }

    public function execute()
    {
        return $this->orderStateLangRepository->findAll();
    }
}