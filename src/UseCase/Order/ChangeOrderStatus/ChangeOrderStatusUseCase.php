<?php

namespace App\UseCase\Order\ChangeOrderStatus;

use App\Repository\OrdersRepository;

class ChangeOrderStatusUseCase
{
    private $ordersRepository;

    public function __construct(OrdersRepository $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
    }

    public function execute(ChangeOrderStatusRequest $request): ChangeOrderStatusResponse
    {
        $order = $this->ordersRepository->find($request->getOrderId());

        if (!$order) {
            return new ChangeOrderStatusResponse(ChangeOrderStatusResponse::STATUS_KO);
        }

        $order->setCurrentState($request->getStatusId());
        $this->ordersRepository->save($order);

        return new ChangeOrderStatusResponse(ChangeOrderStatusResponse::STATUS_OK);
    }
}
