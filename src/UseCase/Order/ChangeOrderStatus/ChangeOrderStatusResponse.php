<?php

namespace App\UseCase\Order\ChangeOrderStatus;

class ChangeOrderStatusResponse
{
    const STATUS_OK = 1;
    const STATUS_KO = -1;

    protected $status;

    public function __construct(int $status)
    {
        $this->status = $status;
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
