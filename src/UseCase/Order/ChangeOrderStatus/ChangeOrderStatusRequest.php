<?php

namespace App\UseCase\Order\ChangeOrderStatus;

class ChangeOrderStatusRequest
{
    protected $orderId;
    protected $statusId;

    public function __construct(int $orderId, int $statusId)
    {
        $this->orderId = $orderId;
        $this->statusId = $statusId;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getStatusId(): int
    {
        return $this->statusId;
    }
}
