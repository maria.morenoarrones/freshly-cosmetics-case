<?php

namespace App\UseCase\Order\GetAllOrders;

use App\Entity\OrderStateLang;
use App\Repository\AddressRepository;
use App\Repository\CountryLangRepository;
use App\Repository\CustomerRepository;
use App\Repository\OrderDetailRepository;
use App\Repository\OrdersRepository;
use App\Repository\OrderStateLangRepository;
use App\Service\Order\OrderBuilder;

class GetAllOrdersUseCase
{
    private const DEFAULT_STATUS = [OrderStateLang::PENDING_STATUS, OrderStateLang::ACCEPTED_PAYMENT_STATUS];
    private $orderBuilder;
    private $ordersRepository;

    public function __construct(
        OrderBuilder     $orderBuilder,
        OrdersRepository $ordersRepository
    )
    {
        $this->orderBuilder = $orderBuilder;
        $this->ordersRepository = $ordersRepository;
    }

    public function execute(bool $showAll)
    {
        if ($showAll) {
            $orders = $this->ordersRepository->findAll();
        } else {
            $orders = $this->ordersRepository->findByOrderStatusId(self::DEFAULT_STATUS);
        }
        return $this->orderBuilder->build($orders);
    }
}
