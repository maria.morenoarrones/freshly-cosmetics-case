# Freshly Cosmetics Case
## Inicializar el proyecto
1. Clonar el proyecto
```
https://gitlab.com/maria.morenoarrones/freshly-cosmetics-case.git
```
2. Acceder al directorio del proyecto e instalar las dependencias
```
composer install
```
3. Levantar el contenedor que contiene la base de datos inicializada
```
docker-compose up -d
```
4. Iniciar el web server local de Symfony
```
symfony server:start
```

## Aplicación
### Página principal
Se muestran solo los pedidos con estado "Preparación en proceso" o "Pago aceptado".
![Página principal](./images/index.png)

### Botón "Show all orders"
Al hacer click en el botón "Show all orders", se muestran todos los pedidos.
![Show all](./images/show_all_orders.png)

### Order detail modal
Al hacer click sobre un pedido, se abre un modal con los detalles del pedido. Para cerrarlo, solo hace falta hacer click fuera del modal.
![Detail modal](./images/order_detail_modal.png)

### Filtro por país
Cuando se filtra por país, solo se muestran los pedidos con envío a dicho país.
![Filter by country](./images/filter_by_country.png)

### Filtro por texto
Se puede buscar cualquier texto en cualquier columna de la tabla usando el buscador de texto "Search".
![Filter by text](./images/datatable-filter.png)

### Cambio de estado
Se puede modificar el estado de un pedido desde la tabla, seleccionado el estado deseado en el desplegable.
